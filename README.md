# Consul NGiNX A/B testing reference app

This reference generates NGiNX configurations suitable for A/B testing.

Register your service to Consul with a tag. consul-template will create a partial
NGiNX configuration (`split_clients` and `upstream` entries) and use the Docker API
to reload NGiNX.

I recommend [Registrator](https://github.com/gliderlabs/registrator) to auto-register services running in Docker to Consul.
There is an included `registrators.yml` as an example.

## DockerCon 2016

This was demoed at DockerCon! Link to slides/video coming soon.

## Start

```
docker-compose up
```

## Start on UCP/Swarm

Modify `constraint:node` entries in `docker-compose.prod.yml` to suit your environment.

```
docker-compose -f docker-compose.prod.yml up
```

## Use A/B admin UI to adjust traffic

[A/B Admin UI](https://gitlab.com/brett.timperman/ab-admin)

## Run tests

```
./test/run.sh
```
