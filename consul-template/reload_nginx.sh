#!/bin/sh

echo "Testing NGiNX configuration"
docker exec nginx nginx -t
RESULT=$?

if [ "$RESULT" == "0" ]; then
  echo "Sending reload signal"
  docker exec nginx nginx -s reload
fi
