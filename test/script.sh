#!/bin/sh

CONSUL_ADDR="${CONSUL_ADDR:-127.0.0.1:8500}"

sleep 3

echo "loading split data"
curl -s -X PUT -d 'a-hash' http://$CONSUL_ADDR/v1/kv/split/a/hash
curl -s -X PUT -d 'a' http://$CONSUL_ADDR/v1/kv/split/a/values/10
curl -s -X PUT -d 'b' http://$CONSUL_ADDR/v1/kv/split/a/values/20
curl -s -X PUT -d 'c' http://$CONSUL_ADDR/v1/kv/split/a/default

curl -s -X PUT -d 'b-hash' http://$CONSUL_ADDR/v1/kv/split/b/hash

echo ""
echo "loading services"
curl -s -X PUT -d '{ "ID": "1", "Name": "redis", "Tags": ["master"], "Address": "127.0.0.1", "Port": 8000 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "2", "Name": "redis", "Tags": ["master"], "Address": "127.0.0.2", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "3", "Name": "web", "Tags": ["0.1"], "Address": "127.0.0.3", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "4", "Name": "web", "Tags": ["0.2"], "Address": "127.0.0.4", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "5", "Name": "web", "Tags": ["0.1","0.2"], "Address": "127.0.0.5", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "6", "Name": "untagged", "Address": "127.0.0.6", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register

curl -s -X PUT -d '{ "ID": "aa", "Name": "a", "Tags": ["a"], "Address": "127.0.0.6", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "ab", "Name": "a", "Tags": ["b"], "Address": "127.0.0.7", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "ac", "Name": "a", "Tags": ["c"], "Address": "127.0.0.8", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register

curl -s -X PUT -d '{ "ID": "ba", "Name": "b", "Address": "127.0.0.9", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "bb", "Name": "b", "Address": "127.0.0.10", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register
curl -s -X PUT -d '{ "ID": "bc", "Name": "b", "Address": "127.0.0.11", "Port": 8080 }' http://$CONSUL_ADDR/v1/agent/service/register

echo "waiting for split.conf to exist"
until [ -f /test/split.conf ]
do
     sleep 1
done

echo "split.conf exists:"
cat /test/split.conf

sleep 1

diff -E -b -w -B /test/split.conf /test/expected-split.conf
RESULT=$?

rm /test/split.conf

exit $RESULT
