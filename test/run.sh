#!/bin/sh

docker-compose \
  -f docker-compose.yml \
  -f docker-compose.test.yml \
  up --abort-on-container-exit

RESULT=$?

docker-compose \
  -f docker-compose.yml \
  -f docker-compose.test.yml \
  rm -vf

exit $RESULT
