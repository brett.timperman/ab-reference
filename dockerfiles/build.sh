#!/bin/sh

docker build -t btimperman/ab-conf -f dockerfiles/Dockerfile.conf .
docker build -t btimperman/ab-nginx -f dockerfiles/Dockerfile.nginx .
docker build -t btimperman/ab-template -f dockerfiles/Dockerfile.template .
