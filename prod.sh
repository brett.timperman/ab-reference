#!/bin/sh

docker rm -f consul
docker rm -f template
docker rm -f nginx
docker rm -f conf

docker pull btimperman/ab-conf
docker pull btimperman/ab-nginx
docker pull btimperman/ab-template

docker run -d --name consul \
  -p 8400:8400 -p 8500:8500 -p "8600:53/udp" \
  progrium/consul -server -bootstrap -ui-dir /ui

docker run -d --name conf btimperman/ab-conf

docker run -d --name nginx -p 4280:80 \
  --volumes-from conf \
  btimperman/ab-nginx

docker run -d --name template \
  --link consul:consul \
  --volumes-from conf \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  -e REQUIRED_UPSTREAMS=root \
  -e DOMAIN_NAME=btim.pe \
  btimperman/ab-template
